%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Q = INVERSEKINEMATIC_Yeetmater6000(robot, T)	
%   Solves the inverse kinematic problem for the Yeetmaster6000 robot
%   where:
%   robot stores the robot parameters.
%   T is an homogeneous transform that specifies the position/orientation
%   of the end effector.
%
%   A call to Q=INVERSEKINEMATIC__Yeetmaster6000 returns 4 possible solutions, thus,
%   Q is a 6x8 matrix where each column stores 6 feasible joint values.
%
%   
%   Example code:
%
%   epson=load_robot('EPSON', 'Prosix_C3_A601C');
%   q = [0 0 0 0 0 0];	
%   T = directkinematic(epson, q);
%   %Call the inversekinematic for this robot
%   qinv = inversekinematic(epson, T);
%   check that all of them are feasible solutions!
%   and every Ti equals T
%   for i=1:8,
%        Ti = directkinematic(epson, qinv(:,i))
%   end
%	See also DIRECTKINEMATIC.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2012, by Arturo Gil Aparicio
%
% This file is part of ARTE (A Robotics Toolbox for Education).
% 
% ARTE is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% ARTE is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License
% along with ARTE.  If not, see <http://www.gnu.org/licenses/>.
function [q] = inversekinematic_Yeetmaster6000(robot, T)
q=zeros(6,4);

%Evaluate the parameters
d = eval(robot.DH.d)
a = eval(robot.DH.a)

%See geometry at the reference for this robot
L5=d(5);

%T= [ nx ox ax Px;
%     ny oy ay Py;
%     nz oz az Pz];
Px=T(1,4);
Py=T(2,4);
Pz=T(3,4);

%Compute the position of the wrist, being W the Z component of the end effector's system
W = T(1:3,3) % b

% Pm: wrist position
%Pm = [Px Py Pz]' - L5*W % [x4 y4 z4]

theta1 = atan2(T(1,3),T(2,3));
Pm = [Px-L5*sin(theta1) Py-L5*cos(theta1) Pz]

q1=Pz;

theta2 = atan(sqrt((4*(a(2)^2) - Pm(1)^2 - Pm(2)^2)/(Pm(1)^2 + Pm(2)^2)))%acos((a(2)^2 - a(3) ^2 + Pm(2)^2 + Pm(1)^2)/(2*a(2)*sqrt(Pm(2)^2 + Pm(1)^2)));

q2 = atan2(Pm(2),Pm(1));
q2 = [q2 - theta2, q2 + theta2];

theta3 = pi - 2*theta2;

q3 = [pi-theta3, theta3-pi];

theta4 = [q2(1)+q3(1), q2(2)+q3(2)];

q4 = theta1 - theta4; % 1x2 matrix

q5 = atan(sqrt(T(2,1)^2 + T(1,1)^2)/T(3,1));
q5rot = -pi*sign(q5);


% four possible solutions, with a wrist up or wrist down solution @ q3, and
% one of two possible rotations of link 5 (the tool will have 180 degree
% rotational symmetry
q = [q1 q1 q1 q1;
    q2(1) q2(2) q2(1) q2(2);
    q3(1) q3(2) q3(1) q3(2);
    q4(1) q4(2) q4(1) q4(2);
    q5 q5 q5+q5rot q5+q5rot];

%q = real(q);
end
