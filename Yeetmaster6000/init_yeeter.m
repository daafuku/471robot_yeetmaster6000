clc;clear

robot = load_robot('471Robot', 'Yeetmaster6000');

%% Question 7
% compute Jacobian matrix of toolpoint when it picks up a M3 screw

% p_M3 = [-1.0625,-0.200,0.500];
p_M3 = [0.2 0.5 -0.10625];
T_M3 = [1 0 0 0; 0 1 0 0; 0 0 1 0; p_M3 1]';


robot.graphical.draw_axes = 0;

q = inversekinematic_Yeetmaster6000(robot, T_M3);
q = q(:,1);

drawrobot3d(robot, q)

compute_jacobian(robot, q)

