%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   PARAMETERS Returns a data structure containing the parameters of the
%   Yeetmaster
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2012, by Arturo Gil Aparicio
%
% This file is part of ARTE (A Robotics Toolbox for Education).
% 
% ARTE is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% ARTE is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License
% along with ARTE.  If not, see <http://www.gnu.org/licenses/>.
function robot = parameters()

robot.name= 'Yeetmaster6000';

%Path where everything is stored for this robot
robot.path = 'robots/ECTE471/Yeetmaster6000';

robot.DH.theta= '[0 q(2)+pi q(3) q(4)+pi/2 q(5)]';
robot.DH.d='[q(1) 0 0 0 0.040]';
robot.DH.a='[-0.100 1.010 1.010 0 0]';
robot.DH.alpha='[0 0 0 pi/2 0]';
robot.J=[];

robot.inversekinematic_fn = 'inversekinematic_Yeetmaster6000(robot, T)';

%number of degrees of freedom
robot.DOF = 5;

%rotational: 0, translational: 1
robot.kind=['T' 'R' 'R' 'R' 'R'];

%minimum and maximum rotation angle in rad
robot.maxangle =[deg2rad(-0.250) deg2rad(1.400); %Axis 1, minimum, maximum translation (still in radians for some reason????)
                deg2rad(-90) deg2rad(90); %Axis 2, minimum, maximum
                deg2rad(-173.5) deg2rad(173.5); %Axis 3
                deg2rad(-173.5) deg2rad(173.5); %Axis 4: 
                deg2rad(-180) deg2rad(180)]; %Axis 5

%maximum absolute speed of each joint rad/s or m/s
robot.velmax = [deg2rad(1); %Axis 1, rad/s
                deg2rad(6); %Axis 2, rad/s
                deg2rad(6); %Axis 3, rad/s
                deg2rad(6); %Axis 4, rad/s
                deg2rad(6)];%Axis 5, rad/s
            
robot.accelmax=robot.velmax/0.1; % 0.1 is here an acceleration time
            
% end effectors maximum velocity
robot.linear_velmax = 1.0; %m/s, unavailable from datasheet

%base reference system 
robot.T0 = [0 -1 0 -0.300;
            0 0 1 0;
            -1 0 0 0.400;
            0 0 0 1];

%INITIALIZATION OF VARIABLES REQUIRED FOR THE SIMULATION
%position, velocity and acceleration
robot=init_sim_variables(robot);


% GRAPHICS
robot.graphical.has_graphics=1;
robot.graphical.color = [225 20 40]./255;
%for transparency
robot.graphical.draw_transparent=0;
%draw DH systems
robot.graphical.draw_axes=1;
%DH system length and Font size, standard is 1/10. Select 2/20, 3/30 for
%bigger robots
robot.graphical.axes_scale=1;
%adjust for a default view of the robot
robot.axis=[-3 3 -3 3 -3 3];
%read graphics files
robot = read_graphics(robot);

%DYNAMICS
robot.has_dynamics=0;